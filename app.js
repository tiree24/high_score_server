const express = require("express");
const app = express();
const port = 3000;
const bodyParser = require("body-parser");
app.use(bodyParser.json());

app.get("/scores", (req, res) =>
  res.send(JSON.stringify(scores.sort((a, b) => (a.score > b.score ? -1 : 1)).slice(0, 3))),
);

app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`));

var scores = [
  { name: "Edwin", score: 50 },
  { name: "David", score: 39 },
  { name: "Mike", score: 3 },
];

// app.all('/user/:id/:op?', function(req, res, next){
//   req.user = users[req.params.id];
//   if (req.user) {
//     next();
//   } else {
//     next(new Error('cannot find user ' + req.params.id));
//   }
// });

// app.get("/scores/:id", function (req, res) {
//   res.sendStatus(200 + req.user.name);
// });

app.post("/scores", function (req, res) {
  res.status(201);
  console.log(req.body);
  scores.push(req.body);

  res.end(JSON.stringify(scores));
});

// app.get('/user/:id/edit', function(req, res){
//   res.send('editing ' + req.user.name);
// });

// app.put('/user/:id', function(req, res){
//   res.send('updating ' + req.user.name);
// });

app.get("*", function (req, res) {
  res.send("what???", 404);
});
